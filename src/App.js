import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import store from './store';
import TodoListContainer from './todos/TodoListContainer';

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <Switch>
                        <Route exact path="/todos" component={TodoListContainer}/>
                        <Redirect to="/todos"/>
                    </Switch>
                </BrowserRouter>
            </Provider>
        );
    }
}

export default App;
