import { createActions } from 'create-redux-actions';

export const CreateTodo = createActions('CreateTodo');
export const UpdateTodo = createActions('UpdateTodo', (todo, description) => [description, todo]);
export const DeleteTodo = createActions('DeleteTodo');
