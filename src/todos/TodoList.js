import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Collapse, Empty } from 'antd';
import { bindActionCreators } from 'redux';

import { PriorityType } from '../shared/constants';
import { date as dateFormatter } from '../shared/formatters';

import Todo from './Todo';
import { todosByDate } from './selectors';
import { UpdateTodo, DeleteTodo } from './actions';

import styles from './TodoList.module.scss';

class TodoList extends React.Component {
    static propTypes = {
        todos: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number.isRequired,
            title: PropTypes.string.isRequired,
            text: PropTypes.string,
            priority: PropTypes.oneOf(Object.values(PriorityType)),
            time: PropTypes.string.isRequired,
            alarm: PropTypes.string.isRequired,
        })).isRequired,
    }

    renderTodos(todos) {
        const { actions } = this.props;

        return todos.map((todo, index) => (
            <Todo key={index} todo={todo}
                onChange={actions.UpdateTodo}
                onDelete={actions.DeleteTodo}
            />
        ));
    }

    render() {
        const { todos } = this.props;

        if (todos.length === 0) {
            return <Empty description="There are no tasks, create at least one"/>
        };

        const groupedTodos = todosByDate(todos);

        const panels = Object.keys(groupedTodos).map((date, index) => (
            <Collapse.Panel key={index} className={styles.panel} header={dateFormatter(date)}>
                {this.renderTodos(groupedTodos[date])}
            </Collapse.Panel>
        ));

        return (
            <Collapse className={styles.collapse} bordered={false}>
                {panels}
            </Collapse>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({
        UpdateTodo,
        DeleteTodo,
    }, dispatch),
});

export default connect(null, mapDispatchToProps)(TodoList);
