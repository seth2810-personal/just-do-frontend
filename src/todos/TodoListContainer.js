import React from 'react';
import { reset } from 'redux-form';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';

import TodoList from './TodoList';
import UserActions from './UserActions';
import SideBarMenu from './SideBarMenu';
import TaskCreateForm from './TodoCreateForm';

import { CreateTodo } from './actions';
import { filteredTodosSelector } from './selectors';

import styles from './TodoListContainer.module.scss';

class TodoListContainer extends React.PureComponent {
    onCreateTodo = (model) => {
        const { actions } = this.props;

        actions.CreateTodo(model);
        actions.ResetForm('todo');
    }

    render() {
        const { todos } = this.props;

        return (
            <React.Fragment>
                <aside className={styles.sidebar}>
                    <Link to="/">
                        <img className={styles.logo} src="/images/logo.png" alt="JustDo"/>
                    </Link>
                    <SideBarMenu/>
                </aside>
                <main className={styles.content}>
                    <UserActions/>
                    <div className={styles.todoList}>
                        <TodoList todos={todos}/>
                    </div>
                    <div className={styles.createForm}>
                        <TaskCreateForm onSubmit={this.onCreateTodo}/>
                    </div>
                </main>
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({
        ResetForm: reset,
        CreateTodo,
    }, dispatch),
});

const mapStateToProps = (state) => ({
    todos: filteredTodosSelector(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoListContainer);
