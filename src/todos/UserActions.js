import React from 'react';
import { Dropdown, Button, Menu, Icon } from 'antd';

import { ReactComponent as ButtonIcon } from '../shared/icons/ic_menu.svg';

import styles from './UserActions.module.scss';

export default class UserActions extends React.PureComponent {
    get menu() {
        return (
            <Menu>
                <Menu.Item className={styles.menuItem}>Change password</Menu.Item>
                <Menu.Item className={styles.menuItem}>Sign out</Menu.Item>
            </Menu>
        );
    }

    render() {
        return (
            <Dropdown overlay={this.menu} trigger={['click']}>
                <Button className={styles.button} shape="circle">
                    <Icon className={styles.icon} component={ButtonIcon}/>
                </Button>
            </Dropdown>
        );
    }
}
