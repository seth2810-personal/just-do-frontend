import React from 'react';
import { connect } from 'react-redux';
import { Menu, Collapse } from 'antd';
import { bindActionCreators } from 'redux';

import { PriorityType } from '../shared/constants';
import { SetTodosFilter } from '../filters/actions';

import styles from './SideBarMenu.module.scss';

class SideBarMenu extends React.PureComponent {
    renderHeader(text) {
        return (
            <span className={styles.header}>{text}</span>
        );
    }

    onClick = (event) => {
        const { key } = event;
        const { actions } = this.props;
        const { [key]: priority } = PriorityType;
        const filter = priority ? { priority } : {};

        actions.SetTodosFilter(filter);
    }

    render() {
        const items = Object.keys(PriorityType).map((priority) => {
            return (
                <Menu.Item className={styles.menuItem} key={priority}>
                    <span className={`ant-menu-item-text ${styles.menuItemText}`}>{PriorityType[priority]}</span>
                </Menu.Item>
            );
        });

        return (
            <Collapse className={styles.collapse} defaultActiveKey="priority" bordered={false}>
                <Collapse.Panel key="priority" className={styles.panel} header={this.renderHeader('Priority')}>
                    <Menu className={styles.menu} defaultSelectedKeys={['all']} onClick={this.onClick}>
                        <Menu.Item className={styles.menuItem} key="all">
                            <span className={`ant-menu-item-text ${styles.menuItemText}`}>all</span>
                        </Menu.Item>
                        {items}
                    </Menu>
                </Collapse.Panel>
            </Collapse>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({
        SetTodosFilter,
    }, dispatch),
});

export default connect(null, mapDispatchToProps)(SideBarMenu);
