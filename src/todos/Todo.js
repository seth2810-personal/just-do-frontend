import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Input, Checkbox, Button, Modal } from 'antd';

import { ReactComponent as AlarmIcon } from '../shared/icons/ic_alarm.svg';
import { ReactComponent as TrashIcon } from '../shared/icons/ic_trash.svg';
import { ReactComponent as ClockIcon } from '../shared/icons/ic_clock.svg';
import { PriorityType, AlarmType, AlarmText } from '../shared/constants';

import PriorityPoint from '../shared/PriorityPoint';

import styles from './Todo.module.scss';

export default class Todo extends React.PureComponent {
    static propTypes = {
        todo: PropTypes.shape({
            id: PropTypes.number.isRequired,
            title: PropTypes.string.isRequired,
            text: PropTypes.string,
            priority: PropTypes.oneOf(Object.values(PriorityType)),
            time: PropTypes.string.isRequired,
            alarm: PropTypes.oneOf(Object.values(AlarmType)),
        }).isRequired,
        onChange: PropTypes.func.isRequired,
        onDelete: PropTypes.func.isRequired,
    }

    constructor() {
        super();

        this.state = { checked: false };
    }

    get deleteButton() {
        const { checked } = this.state;

        if (!checked) {
            return null;
        }

        return (
            <Button className={styles.deleteButton} onClick={this.onDelete} shape="circle">
                <Icon className={styles.deleteButtonIcon} component={TrashIcon}/>
            </Button>
        );
    }

    onSelect = (event) => {
        const { target: { checked } } = event;

        this.setState({ checked });
    }

    onConfirmDelete = () => {
        const { todo, onDelete } = this.props;

        onDelete(todo);
    }

    onCancelDelete = () => {
        this.setState({ checked: false });
    }

    onDelete = () => {
        Modal.confirm({
            centered: true,
            okText: 'Delete',
            onOk: this.onConfirmDelete,
            onCancel: this.onCancelDelete,
            title: 'Are you sure you want to delete the task?'
        });
    }

    onTextChange = (event) => {
        const { target: { value: text } } = event;
        const { todo, onChange } = this.props;

        onChange(todo, { text });
    }

    render() {
        const { props: { todo }, state: { checked } } = this;
        const { [todo.alarm]: alarmText } = AlarmText;

        return (
            <div className={styles.container}>
                <div className={styles.checkbox}>
                    <Checkbox checked={checked} onChange={this.onSelect}/>
                </div>
                <div className={styles.content}>
                    <div className={styles.title}>{todo.title}</div>
                    <Input className={styles.textInput} type="text" defaultValue={todo.text}
                        placeholder="Enter description..." disabled={!!todo.text}
                        onPressEnter={this.onTextChange}/>
                    <div className={styles.additionalInfo}>
                        <div className={styles.priority}>
                            <PriorityPoint priority={todo.priority}/>
                            <span className={styles.priorityLabel}>{todo.priority} priority</span>
                        </div>
                        <div className={styles.time}>
                            <Icon className={styles.timeIcon} component={ClockIcon}/>
                            <span>{todo.time}</span>
                        </div>
                        <div className={styles.notification}>
                            <Icon className={styles.notificationIcon} component={AlarmIcon}/>
                            {alarmText}
                        </div>
                    </div>
                </div>
                <div className={styles.actions}>
                    {this.deleteButton}
                </div>
            </div>
        );
    }
}
