import { handleActions } from 'create-redux-actions';

import { CreateTodo, UpdateTodo, DeleteTodo } from './actions';

const findById = (todos, id) => {
    return todos.findIndex((todo) => todo.id === id);
}

const createTodo = (state, action) => {
    const { payload: todo } = action;

    return [...state, { id: state.length + 1, ...todo }];
};

const updateTodo = (state, action) => {
    const { payload: fields, meta: todo } = action;
    const idx = findById(state, todo.id);
    const { [idx]: item } = state;

    return [...state.slice(0, idx), { ...item, ...fields }, ...state.slice(idx + 1)];
};

const deleteTodo = (state, action) => {
    const { payload: todo } = action;
    const idx = findById(state, todo.id);

    return [...state.slice(0, idx), ...state.slice(idx + 1)];
}

export default handleActions({
    [CreateTodo]: createTodo,
    [UpdateTodo]: updateTodo,
    [DeleteTodo]: deleteTodo,
}, []);
