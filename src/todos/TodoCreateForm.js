import React from 'react';
import { Button } from 'antd';
import { Field, reduxForm, propTypes } from 'redux-form'
import { combineValidators, isRequired } from 'revalidate';

import Input from '../forms/fields/Input';
import DatePicker from '../forms/fields/DatePicker';
import TimePicker from '../forms/fields/TimePicker';
import AlarmPicker from '../forms/fields/AlarmPicker';
import PriorityPicker from '../forms/fields/PriorityPicker';

import { PriorityType } from '../shared/constants';

import styles from './TodoCreateForm.module.scss';

const disabledDate = (current) => {
    return current.toDate().getTime() < Date.now();
};

class TodoCreateForm extends React.PureComponent {
    static propTypes = {
        ...propTypes,
    }

    render() {
        const { handleSubmit, invalid } = this.props;

        return (
            <form onSubmit={handleSubmit}>
                <Field name="title" className={styles.input} placeholder="Enter task title..." autoComplete="off" component={Input}/>
                <div className={styles.container}>
                    <div className={styles.fields}>
                        <Field name="date" component={DatePicker} disabledDate={disabledDate}/>
                        <Field name="time" component={TimePicker}/>
                        <Field name="alarm" component={AlarmPicker}/>
                        <Field name="priority" component={PriorityPicker}/>
                    </div>
                    <div>
                        <Button className={styles.sendButton} htmlType="submit" disabled={invalid}>Send</Button>
                    </div>
                </div>
            </form>
        );
    }
}

const validate = combineValidators({
    title: isRequired('Title'),
    date: isRequired('Date'),
    time: isRequired('Time'),
    alarm: isRequired('Alarm'),
    priority: isRequired('Priority'),
})

export default reduxForm({
    validate,
    form: 'todo',
    initialValues: {
        priority: PriorityType.Neutral,
    },
})(TodoCreateForm);
