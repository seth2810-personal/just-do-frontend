import { createSelector } from 'reselect';

import { filtersSelector } from '../filters/selectors';

export const todosSelector = ({ todos }) => todos;

export const todosFiltersSelector = createSelector(
    filtersSelector,
    ({ todos }) => todos,
);

export const filteredTodosSelector = createSelector(
    todosSelector,
    todosFiltersSelector,
    (todos, filters) => {
        return Object.keys(filters).reduce((memo, key) => {
            const { [key]: value } = filters;

            return memo.filter((item) => item[key] === value);
        }, todos);
    }
);

export const todosByDate = (todos) => {
    return todos.reduce((memo, todo) => {
        const { [todo.date]: todos = [] } = memo;
        memo[todo.date] = todos;
        todos.push(todo);
        return memo;
    }, {});
};
