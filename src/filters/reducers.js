import { handleActions } from 'create-redux-actions';

import { SetTodosFilter } from './actions';

const setTodosFilter = (state, { payload }) => ({
    ...state,
    todos: payload,
});

export default handleActions({
    [SetTodosFilter]: setTodosFilter,
}, {
    todos: {}
});
