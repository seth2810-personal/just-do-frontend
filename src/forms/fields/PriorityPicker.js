import React from 'react';
import { Dropdown, Menu } from 'antd';

import { PriorityType } from '../../shared/constants';

import PriorityPoint from '../../shared/PriorityPoint';

import styles from './PriorityPicker.module.scss';

export default class PriorityPicker extends React.PureComponent {
    get menu() {
        const items = Object.keys(PriorityType).map((priority) => (
            <Menu.Item key={PriorityType[priority]} className={styles.menuItem}>
                <PriorityPoint priority={PriorityType[priority]}/>
                <span className={styles.menuItemText}>{PriorityType[priority]}</span>
            </Menu.Item>
        ));

        return (
            <Menu onClick={this.onChange}>{items}</Menu>
        );
    }

    onChange = (event) => {
        const { key } = event;
        const { input: { onChange } } = this.props;

        onChange(key);
    }

    render () {
        const { input: { value } } = this.props;

        if (!value) {
            return null;
        }

        return (
            <Dropdown overlay={this.menu} placement="topCenter" trigger={['click']}>
                <span className="ant-dropdown-link">
                    <PriorityPoint priority={value}/>
                </span>
            </Dropdown>
        );
    }
}
