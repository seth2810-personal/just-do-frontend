import React from 'react';
import { Dropdown, Menu, Icon } from 'antd';

import { AlarmType, AlarmText } from '../../shared/constants';
import { ReactComponent as AlarmIcon } from '../../shared/icons/ic_alarm.svg';

import styles from './AlarmPicker.module.scss';

export default class AlarmPicker extends React.PureComponent {
    get menu() {
        const { input: { value } } = this.props;

        const items = Object.keys(AlarmType).map((alarm) => (
            <Menu.Item key={AlarmType[alarm]} className={styles.menuItem}>
                <span className={styles.menuItemText}>{AlarmText[AlarmType[alarm]]}</span>
            </Menu.Item>
        ));

        return (
            <Menu onClick={this.onChange} selectedKeys={[value]}>{items}</Menu>
        );
    }

    onChange = (event) => {
        const { key } = event;
        const { input: { onChange } } = this.props;

        onChange(key);
    }

    render () {
        return (
            <Dropdown overlay={this.menu} placement="topCenter" trigger={['click']}>
                <span className="ant-dropdown-link">
                    <Icon component={AlarmIcon}/>
                </span>
            </Dropdown>
        );
    }
}
