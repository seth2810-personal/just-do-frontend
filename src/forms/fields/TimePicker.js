import React from 'react';
import { Icon, TimePicker } from 'antd';

import { ReactComponent as ClockIcon } from '../../shared/icons/ic_clock.svg';

export default class AlarmPicker extends React.PureComponent {
    onChange = (time, timeString) => {
        const { input: { onChange } } = this.props;

        onChange(timeString);
    }

    render () {
        const icon = (
            <span className="ant-dropdown-link">
                <Icon component={ClockIcon}/>
            </span>
        );

        return (
            <TimePicker suffixIcon={icon} format="HH:mm" allowClear={false} onChange={this.onChange}/>
        );
    }
}
