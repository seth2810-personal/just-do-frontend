import React from 'react';
import { Icon, DatePicker as DatePickerComponent } from 'antd';

import { ReactComponent as CalendarIcon } from '../../shared/icons/ic_calendar.svg';

export default class DatePicker extends React.PureComponent {
    onChange = (date, dateString) => {
        const { input: { onChange } } = this.props;

        onChange(dateString);
    }

    render () {
        const { label, name, input, ...customProps } = this.props;

        const icon = (
            <Icon component={CalendarIcon}/>
        );

        return (
            <DatePickerComponent suffixIcon={icon} onChange={this.onChange}
                allowClear={false} showTime={false} showToday={false}
                {...customProps}
            />
        );
    }
}
