import React from 'react';
import { Input as InputComponent } from 'antd';

const Input = ({
    meta,
    label,
    input: { name, value, onChange },
    ...customProps,
}) => (
    <InputComponent name={name} value={value} onChange={onChange} {...customProps}/>
);

export default Input;
