import { createStore } from 'redux';

import rootReducer from './reducers';
import { TODOS } from './shared/todos';

export default createStore(rootReducer, { todos: TODOS });
