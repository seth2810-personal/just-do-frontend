import React from 'react';
import PropTypes from 'prop-types';

import { PriorityType } from './constants';

import styles from './PriorityPoint.module.scss';

const classNamesMap = {
    [PriorityType.Urgent]: styles.urgent,
    [PriorityType.Important]: styles.important,
    [PriorityType.Normal]: styles.normal,
    [PriorityType.Neutral]: styles.neutral,
};

export default class PriorityPoint extends React.PureComponent {
    static propTypes = {
        priority: PropTypes.oneOf(Object.values(PriorityType)).isRequired,
    }

    render() {
        const { priority } = this.props;
        const { [priority]: className } = classNamesMap;

        return (
            <span className={`${styles.point} ${className}`}></span>
        )
    }
}
