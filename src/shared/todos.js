import { PriorityType } from './constants';

export const TODOS = [
    {
        id: 1,
        title: 'Выполнить заказ по логотипу',
        text: 'Разработать логотип для компании Z, цвета: белый и синий',
        priority: PriorityType.Urgent,
        date: '2019-01-16',
        time: '13:00',
        alarm: '5m',
    },
    {
        id: 2,
        title: 'Прием у врача',
        text: 'Кабинет №27, Остапенко А. В.',
        priority: PriorityType.Important,
        date: '2019-02-27',
        time: '13:00',
        alarm: '1h',
    },
    {
        id: 3,
        title: 'Сходить в магазин',
        text: null,
        priority: PriorityType.Neutral,
        date: '2019-02-28',
        time: '13:00',
        alarm: '30m',
    }
];
