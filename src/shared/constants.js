export const PriorityType = {
    Urgent: 'urgently',
    Important: 'important',
    Normal: 'normal',
    Neutral: 'neutral',
};

export const AlarmType = {
    FiveMinutes: '5m',
    TenMinutes: '10m',
    HalfAnHour: '30m',
    OneHour: '1h',
    ThreeHours: '3h',
    OneDay: '1d',
    OneWeek: '1w',
};

export const AlarmText = {
    [AlarmType.FiveMinutes]: '5 min.',
    [AlarmType.TenMinutes]: '10 min.',
    [AlarmType.HalfAnHour]: '30 min.',
    [AlarmType.OneHour]: '1 hour',
    [AlarmType.ThreeHours]: '3 hours',
    [AlarmType.OneDay]: '1 day',
    [AlarmType.OneWeek]: '1 week',
};
