import * as moment from 'moment';

export const date = (str) => {
    const date = moment(str);
    const now = moment().endOf('day');
    const diff = moment.duration(date.diff(now));

    console.log(diff);

    if (diff.years() !== 0) {
        return date.format('YYYY MMMM DD');
    } else if (diff.milliseconds() === 1) {
        return `Tomorrow, ${date.format('MMMM DD')}`;
    } else if (diff.days() === 0) {
        return `Today, ${date.format('MMMM DD')}`;
    } else {
        return date.format('dddd, MMMM DD');
    }
}
