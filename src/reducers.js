import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'

import todos from './todos/reducers';
import filters from './filters/reducers';

export default combineReducers({
    todos,
    filters,
    form: formReducer,
});
